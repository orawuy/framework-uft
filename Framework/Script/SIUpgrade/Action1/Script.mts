﻿' Get date time for execute all test

Call FW_StartExecute()
strTag = "Run"

'********************************************************************************
'Initial Start Run
'Create Initial FrameWork	(Define Setup Parameter & Import TestData)
'********************************************************************************
Call FW_CreateInitialFramework()
Call FW_WriteLog("FW_CreateInitialFramework", "OK")
Call FW_WriteLog("FW_CreateInitialFramework", "SUCCESS")
Call FW_WriteLog("StartExecuteTime", startExecuteTime)
If strTag = "" Then
	strTag = "Run"
End If

tag = Split(strTag, ",")
tagArrayCount = UBound(tag)

'Call FW_WriteLog("tag", tag)
'Call FW_WriteLog("tagArrayCount", tagArrayCount)
'Call FW_WriteLog("GetRowCount", DataTable.GlobalSheet.GetRowCount)
Call FW_WriteLog("GetRowCount", DataTable.GetSheet("TestController").GetRowCount)

For dtRow = 1 To DataTable.GetSheet("TestController").GetRowCount Step 1
	DataTable.GetSheet("TestController").SetCurrentRow(dtRow)
	tagAction = UCase(DataTable("Tag", "TestController"))
	testCase = Trim((DataTable("TestCaseNo", "TestController")))
	testName = Trim((DataTable("TestCaseName", "TestController")))
	
	Call FW_WriteLog("Tag", tagAction)
	Call FW_WriteLog("TestCaseNo", testCase)
	Call FW_WriteLog("TestCaseName", testName)
	
	For index = 0 to tagArrayCount Step 1
		If UCase(Trim(tagAction)) = "RUN" Then
			DataTable(RESULT_STATUS, "TestController") = "Run"
		
			'Define Parameter for Create Folder TestCase for Get Snapshot per Test Case
			strResultPathTestCase = strResultPathExecution
			
			'Create Folder before Run
			Call CreateFolder(strResultPathTestCase)
			Call FW_WriteLog("Row Execution " & DataTable.GetSheet("TestController").GetCurrentRow & VbNewLine & "StartTime : " & Now() & VbNewLine & "TestCase : " & testCase & VbNewLine & "TestName : " & testName & VbNewLine & "TestCase Result (CaptureScreenShot) : " & strResultPathTestCase, "")
			
			'Call Function from DataTable Column TestCase
			RUNNING_STEP = 1
			Select Case testCase
				'TC001	
				Case "TC001"
					RunAction "TC001 [TC001_CAL_CarPrice]", oneIteration
				'TC002	
				Case "TC002"
					RunAction "TC002 [TC002_CAL_MonthlyInstallments]", oneIteration
				'TC003	
				Case "TC003"
					RunAction "TC003 [TC003_CAL_Conditinal]", oneIteration
			End Select
		
			'Call Function Stamp Result from Report to Datatable Column 'Desctiption' 			
			Call FW_CreateResult(dtRow)

			Exit For
			
'		ElseIf index = tagArrayCount Then
'			'Create Status not run 
'			DataTable(RESULT_STATUS,dtGlobalSheet) =  "No Run"
'			Exit For
		Else
'			DataTable(RESULT_STATUS, dtGlobalSheet) = tagAction & " <> " & tag(index) & " No Run"
			DataTable(RESULT_STATUS, "TestController") =  "No Run"
		End If
	Next
Next

'Export Result from Datatable
Call FW_StopExecute()
Call FW_WriteLog("FW_StopExecute", "OK")
Call FW_WriteLog("FW_StopExecute", "SUCCESS")
Call FW_WriteLog("StopExecuteTime", stopExecuteTime)

Call FW_ExportResult()
'Print "Execution Completed [" & Now & "]"
Call FW_WriteLog("Execution Completed", "[" & Now & "]")
'FW_ExportReportToDoc()
'FW_StopExecute()
'********************************************************************************
